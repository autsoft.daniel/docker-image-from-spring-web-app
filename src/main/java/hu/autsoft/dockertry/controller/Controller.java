package hu.autsoft.dockertry.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class Controller {

    @GetMapping("/")
    public String index(@RequestHeader("user-agent") String userAgent) {
        System.out.println(userAgent);
        return "Greetings from Spring Boot!";
    }

}
