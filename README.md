## Docker files multiple building approach

### Image from application binary

    docker build . -t hello:springweb-binary -f ./Dockerfile_binary

### Image from application source with Maven build

    docker build . -t hello:springweb-build -f ./Dockerfile_build

### Image from application source from Git with Maven build

    docker build . -t hello:springweb-git -f ./Dockerfile_git

**If the repo would be private, access credentials needs to be added in --build-arg** 

    docker build . -t hello:springweb-git -f ./Dockerfile_git --build-arg username=your_user_name --build-arg password=your_password

